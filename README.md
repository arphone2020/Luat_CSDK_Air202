Luat_CSDK_Air202语言开发环境，用于air202模块进行C语言二次开发，丰富的api接口。更多内容查阅doc目录



V1.1修改记录
-----------------------
1. 增加timer，fs，flash，gpio，alarm，uart，audio 接口对应demo
2. 增加udp收发demo
3. 修改RAM空间为1MB
4. 增加iot_debug_set_fault_mode接口，用来设置软件异常情况下，是否进入调试模式
5. 增加iot_audio_rec_start/stop录音接口
6. 解决tcp/udp连续发送导致的死机问题
7. 更新《Luat_IOT_SDK_C语言下载调试手册》《Luat_IoT_SDK_C语言编程手册》


V1.2修改记录
-----------------------
1. 修改udp连续发送死机问题
2. 修改select对端关闭后，没有返回的问题
3. 解决服务器关闭时，依然能链接成功的问题
4. 合入RDA8955_W17.27.5_IDH
5. 增加FTP应用
6. 增加OTA（远程升级APP）demo
7. 更新《Luat_IOT_SDK_C语言下载调试手册》《Luat_IoT_SDK_C语言编程手册》


V1.3修改记录
-----------------------
1. 基础版本升级
2. 解决录音不连续的问题
3. 增加demo_minisystem项目
4. 增加默认./demo_minisystem_B4245.lod文件，用来测试验证
5. 更新《Luat_IoT_SDK_C语言环境安装步骤》《Luat_IoT_SDK_C语言编程手册》

V1.3.1修改记录
-----------------------
1. 增加虚拟AT接口iot_vat_init、iot_vat_sendATcmd以及相关例子

V1.3.2修改记录
-----------------------
1. 修改recv接口返回53的问题
2. 修改串口接收丢数据的问题

V1.3.3修改记录
-----------------------
1. 修改socket close之后网络状态变回READY的问题
2. 修改network模块和TCPIP AT指令同时使用时，网络状态的问题
   修改后network和AT指令的网络(pdp)状态分开维护，互不影响
3. 网络状态增加了OPENAT_NETWORK_GOING_DOWN状态，表示网络(pdp)正在去激活状态
4. 增加mqtt应用

V1.4修改记录
-----------------------
1. 基础版本升级到V4719
2. 使用新的文件系统，改善flash擦写，文件保存的延迟
3. 增加SSL应用

V1.4.1修改记录
-----------------------
1. 增加getsockopt接口
2. 支持iot_flash_erase接口支持4KB对其擦除
3. 更新《Luat_IoT_SDK_C语言编程手册》

V1.4.2修改记录
-----------------------
1.基础版本升级到V4837
2. 修改多task socket select操作可能导致task被阻塞

V1.5修改记录
-----------------------
1. 基础版本升级到V5001
2. 更换编译器CSDTK4，兼容WIN10系统，从本版本开始，编译器必须使用CSDTK4

V1.5.1修改记录
-----------------------
1. 基础版本升级到V5021
2. 新增platfrom/IoT_SDK_UI项目
3. 支持iot_camera.h接口(仅基于platfrom/IoT_SDK_UI项目可用)
4. 新增iot_lcd.h接口(仅基于platfrom/IoT_SDK_UI项目可用)
5. 新增iot_touchscreen.h(仅基于platfrom/IoT_SDK_UI项目可用)，
6. 新增iot_keypad接口
7. 新增demo_ui
8. 新增demo_lcd
8. 新增demo_camera

V1.5.2
1. 升级基础版本V5135
2. 添加SPI 全双工不带CS功能
3. 添加UART波特率UART_BAUD_10400

V1.5.3
1. 升级基础版本到5163
2. 修改串口缓存buff分配失败, 导致串口初始化失败的问题

V1.5.4
1. 升级基础版本到5243
2. 修改摄像头不兼容问题,iot_camera_init接口添加了摄像头pin配置,摄像头分频设置, 摄像头SPI模式, 摄像头输出格式的设置

V1.5.5
1. 升级基础版本到5244
2. 修改uart_close导致重启的问题

V1.6
1. 升级基础版本到5266
2. 添加虚拟sim卡功能, 
3. 将内存由0X100000改成0XC0000

V1.6.1
1. IOT_SDK_UI 升级基础版本5269
2. IOT_SDK_UI 添加虚拟sim卡功能
3. IOT_SDK_UI 添加bootloader串口下载功能

V1.6.2
1. IOT_SDK_UI,IOT_SDK基础版本升级到5387
2. IOT_SDK_UI,IOT_SDK 更新VSIM库

V1.7
1. IOT_SDK_UI,IOT_SDK基础版本升级到5408
2. 添加二维码解码项目DEMO_ZBAR

V1.7.1
1. IOT_SDK_UI,IOT_SDK基础版本升级到5421
2. 添加zbar多种编码解析格式如下:
		EAN-13/UPC-A, UPC-E, EAN-8, Code 128, Code 39, Interleaved 2 of 5 and QR Code.
		
V1.8
1.基础版本升级到5437 
1.platform下添加支持tts的lod版本, 添加对应的demo_tts项目,
	(注:IOT_SDK_TTS对比IOT_SDK: 增加了tts, 来电铃音播放, 缩小了文件系统大小, OPENAT ROM空间)
2.添加基站查询功能通过AT+AMGSMLOC
V1.8.1
1.IOT_SDK_UI基础版本升级到5438 
2.添加camera采集数据上报
V1.8.2
1.IOT_SDK IOT_SDK_TTS基础版本升级到5438 
V1.8.3
1. 添加math库
V1.8.4
1. 在doc目录下添加C_SDK开发相关工具下载链接文件
V1.8.5
1. 添加demo_wdt
V1.8.6
1. 添加app_http项目
2. 添加demo_gps_and_IMEI项目
3. 解决demo ota死机问题
4. 目录有横杠-，lodtobin工具运行报错
V.1.8.7
1. 升级基础版本到5475
2. 字符集为IRA模式发送一些特殊字符，接收到的字符乱码
3. 修复在TE字符集为"GSM"时，读取中文短信出现乱码的问题
V.1.9.1
1. 整理doc下的文档, 添加编译调试环境安装步骤.pdf
V.1.9.2
1. 升级基础版本到5487
2. 修复短信发送多个特殊字符时模块重启问题
V.1.9.3
1. 升级基础版本到5497
2. 修改底层定义局部数组较大容易造成栈溢出的问题
V.1.9.4
1. 升级基础版本到V5606
2. 添加demo_ui_camera项目(必须使用64M flash, 支持tts和camera功能)
3. 优化spi接口
V1.9.5
1. 升级基础版本到V5638
2. 所有项目新增PCM功能
V1.9.6
1. 升级基础版本到V5696
2. 修复UART硬件流控BUG
3. 修复socket send在少数情况下无法返回的问题
V1.9.7
1. 升级基础版本到V6070
2. 修改的MP3,PCM等文件播放结尾有噗噗声的问题

V1.9.8
1.加入26MHz 时钟输出功能

V1.9.9
1.升级基础版本到V6121
2.解决TTS播放时出现爆破音的问题
3.解决TTS音质问题
V2.0.0
1，增加iot_os_mem_used接口
2，增加iot_fs_get_fs_info、iot_fs_find_first、iot_fs_find_next、iot_fs_find_close接口
3，增加IOT_SDK_ZY_4M/4M_TTS/8M项目
V2.0.1
1. 添加iot_lcd_close接口，并在demo_zbar项目中添加lcd pin功能切换测试