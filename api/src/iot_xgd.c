#include "iot_xgd.h"
#include "iot_fs.h"

/*******************************************
**                 BASE                   **
*******************************************/

#define ddi_print iot_debug_print 

/**系统阻塞延时
*@param		nMs:延时时间, 单位:毫秒
*@return	DDI_OK: 成功
*           DDI_ERR: 失败
**/
s32 ddi_sys_msleep(u32 nMs)
{
	iot_os_sleep(nMs);

	return DDI_OK;
}

/**获取一个软定时器的当前时间
*@param		void
*@return	>0:获取成功，返回软定时器的当前时间值
*           DDI_ERR:获取失败
*			备注：把本接口的返回值传递给 ddi_utils_stimer_query,用于查询是否超时。
**/

s32 ddi_utils_stimer_get(void)
{
	s32 stimer;
	
	stimer = iot_os_get_system_tick();

	ddi_print("[ddi-%s] stimer %d", __FUNCTION__, stimer);
	return stimer;
}

/**查询从指定时间起,是否已经超时
*@param		stimer:软定时器的时间
*@param		timeout_ms:超时时间
*@return	0：未超时
*           1：已超时
*			备注：本接口的 stimer 可通过 ddi_utils_stimer_get 接口获取
**/

s32 ddi_utils_stimer_query(s32 stimer, u32 timeout_ms)
{
	#define DDI_RUN_TICK_VALUE(cur,org) (((cur) >= (org)) ? ((cur)-(org)):((~0) - (org) + (cur)))
	#define DDI_TIME1S_TICK (16384)
	s32 curStimer = iot_os_get_system_tick();
	s32 runTick = DDI_RUN_TICK_VALUE(curStimer, stimer);

	ddi_print("[ddi-%s] run_time %d, timeout %d", __FUNCTION__, runTick*1000/DDI_TIME1S_TICK, timeout_ms);
	if (runTick*1000/DDI_TIME1S_TICK >  timeout_ms)
	{
		return 1;
	}

	return 0;
}

/**申请动态内存,且内存会被初始化为0x00(注意:申请的内存如果不再使用,必须显式地释放)
*@param		size:申请的内存大小,单位字节
*@return	非NULL:申请成功
*           NULL:申请失败
**/

void *ddi_mem_malloc(u32 size)
{
	char *p = iot_os_malloc(size);

	if (!p)
	{
		return NULL;
	}
	
	memset(p, 0, size);
	return p;
}

/**释放已申请的动态内存(注意:释放以后的动态内存不能再使用)
*@param		ptr:指向申请的动态内存
*@return	void
**/

void ddi_mem_free(void *ptr)
{
	iot_os_free(ptr);
	ptr = NULL;
}


/*******************************************
**                 socket                 **
*******************************************/

/**创建一个 socket 句柄,申请资源,并初始化相关环境
*@param		sock_type_t
*@return	>0:成功 socke 句柄
*			DDI_ERR:失败
**/

s32 ddi_socket_new(sock_type_t type)
{
	s32 handle;

	if (type == SOCK_TYPE_TCP)
	{
		handle = socket(AF_INET,SOCK_STREAM,0);	  
	}
	else
	{
		handle = socket(AF_INET,SOCK_DGRAM,0);
	}

	ddi_print("[ddi-%s] type %d,handle %d", __FUNCTION__, type, handle);
	
	if (handle < 0)
		return DDI_ERR;
	
	return handle;
}

/**释放 socket 句柄,释放资源
*@param		id:socket 句柄
*@return	DDI_OK:成功
*			DDI_EINVAL:非法参数
**/

s32 ddi_socket_free(s32 id)
{
	if (!id)
		return DDI_EINVAL;

	close(id);

	return DDI_OK;
}


/**解析域名,返回对应的 IP 地址
*@param		socket 句柄
*@param		host:待解析的域名地址,以’\0’结尾的字符串
*@param		sock_addrs_t:addrs
*@param		size:传入的’addrs’个数
*@param		timeout_ms:超时时间,单位毫秒
*@return	DDI_OK:成功
*			DDI_ERR:失败
*			DDI_ETRANSPORT:连接失败
*			DDI_ETIMEOUT:超时
**/

s32 ddi_socket_get_addr(void *ctx, const s8 *host, 
			sock_addrs_t *addrs, 
				s32 size, s32 timeout_ms)
{
    struct hostent *hostentP = NULL;
    char *ipAddr = NULL;
	
	if (!host || !addrs || !(size > 0))
	{
		return DDI_ERR;
	}

	ddi_print("[ddi-%s] host %s", __FUNCTION__, host);
	
    //获取域名ip信息
    hostentP = gethostbyname(host);

    if (!hostentP)
    {
        return DDI_ERR;
    }

    // 将ip转换成字符串
    ipAddr = ipaddr_ntoa((const ip_addr_t *)hostentP->h_addr_list[0]);

	memset(addrs, 0, sizeof(sock_addrs_t));
	memcpy(addrs[0].ip, ipAddr, strlen(ipAddr));
	
    ddi_print("[ddi-%s] host=> IP %s,%s", __FUNCTION__, ipAddr, addrs[0].ip);

	return DDI_OK;
}

/**建立 socket 连接
*@param		id:socket 句柄
*@param		ipaddr:服务器 IP 地址
*@param		port:服务器端口号
*@param		timeout_ms:超时时间，单位毫秒
*@return	DDI_OK:成功
*			DDI_EINVAL:IP 地址是非法参数
*			DDI_ETRANSPORT:连接失败
*			DDI_ETIMEOUT:超时
*			备注:同一个 socket 句柄 建立连接后，必须先断开连接，才能再次建立连接
**/
s32 ddi_socket_connect(s32 id, const s8 *ipaddr, s32 port, s32 timeout_ms)
{

	struct sockaddr_in socket_addr; 
    s32 connErr;
	
    // 建立TCP链接
    memset(&socket_addr, 0, sizeof(socket_addr)); // 初始化服务器地址  
    socket_addr.sin_family = AF_INET;  
    socket_addr.sin_port = htons((unsigned short)port);  
    inet_aton(ipaddr,&socket_addr.sin_addr);

    connErr = connect(id, (const struct sockaddr *)&socket_addr, sizeof(struct sockaddr));

	ddi_print("[ddi-%s] %s:%d connErr %d", __FUNCTION__, ipaddr, port, connErr);
	if (connErr < 0)
	{
		return DDI_ERR;
	}

	return DDI_OK;
}

/**建立 断开 socket 连接
*@param		id:socket 句柄
*@return	DDI_OK:成功
*@return	DDI_ERR:失败
**/

s32 ddi_socket_disconnect(s32 id)
{
	s32 err = close(id);

	ddi_print("[ddi-%s] id %d err %d", __FUNCTION__, id, err);
	if (err < 0)
		return DDI_ERR;

	return DDI_OK;
}


/**向对端发送数据
*@param		id:socket 句柄
*@param		buf:待发送的数据
*@param		len:数据长度
*@return	>=0:实际发送的数据长度
*			DDI_EINVAL:非法参数
*			DDI_ETRANSPORT:发送失败
*			DDI_EIO:未建立连接
**/

s32 ddi_socket_write(s32 id, const u8 *buf, s32 len)
{
	s32 err;
	
	err = send(id, buf, len, 0);

	ddi_print("[ddi-%s] id %d err %d", __FUNCTION__, id, err);
	
	if (err < 0)
		return DDI_ERR;

	return err;
}

/**从对端接收数据
*@param		id:socket 句柄
*@param		buf:缓冲区大小
*@param		len:数据长度
*@param		timeout_ms:超时时间,单位毫秒,0 表示立即返回,大于 0 表示阻塞等 数 待时 间
*@return	>=0:实际发送的数据长度
*			DDI_EINVAL:非法参数
*			DDI_ETRANSPORT:发送失败
*			DDI_EIO:未建立连接
**/


s32 ddi_socket_read(s32 id, u8 *buf, s32 len, s32 timeout_ms)
{
	s32 err;
	fd_set readset;
	s32 ret;
	struct timeval tv;
	
	FD_ZERO(&readset);
    FD_SET(id, &readset);

	tv.tv_sec  = timeout_ms/1000;
	tv.tv_usec = (timeout_ms%1000)*1000;
	
    ret = select(id+1, &readset, NULL, NULL, &tv);
	ddi_print("[ddi-%s] come in", __FUNCTION__);
	if(ret > 0)
    {
    	ddi_print("[ddi-%s] id %d, tv_sec %d, tv_usec %d, ret %d",
				__FUNCTION__, id, tv.tv_sec, tv.tv_usec, ret);
    	if (FD_ISSET(id, &readset))
    	{
			err = recv(id, buf, len, 0);
			
			if (err < 0)
				return DDI_ERR;
		}
		else
		{
			ddi_print("[ddi-%s] ret %d else", __FUNCTION__, ret);
			return DDI_ERR;
		}
    }
	else if (ret == 0)
	{
		ddi_print("[ddi-%s] ret %d DDI_ETIMEOUT", __FUNCTION__, ret);
		return DDI_ETIMEOUT;
	}
	else
	{
		ddi_print("[ddi-%s] ret %d DDI_ERR", __FUNCTION__, ret);
		return DDI_ERR;
	}
	ddi_print("[ddi-%s] id %d, len %d, timeout %d err %d", __FUNCTION__, id, len, timeout_ms, err);
	
	return err;
}

/**打开或创建文件
*@param		lpPath:文件绝对路径
*@param		lpMode:打开或创建文件模式
*"w"/"w+": 如果文件不存在则创建，存在则清空。
*"wb"/"wb+": 如果文件不存在则创建。
*"rb": 如文件不存在，返回失败。
*@return	=0:打开文件失败
*			>0 : 文件句柄
*
**/

s32 ddi_vfs_open(const u8* lpPath,const u8* lpMode)
{
    s32 ret = -1;
    UINT32 iFlag;
    if((strcmp(lpMode,"wb") == 0) || (strcmp(lpMode,"wb+") == 0)){
        iFlag =  O_WRONLY|O_CREAT;       
    }
    else if((strcmp(lpMode,"w") == 0) || (strcmp(lpMode,"w+") == 0)){
        iFlag =  O_WRONLY|O_CREAT;
    }
    else if(strcmp(lpMode,"rb")){
        iFlag =  O_RDWR|O_CREAT;
    }
    ret = iot_fs_open_file(lpPath,iFlag);
    if (ret < 0){
        return 0;
    }
    return ret;
}

/**关闭文件
*@param		lpPath:文件句柄
*@return	DDI_ERR: 失败
*			DDI_OK : 成功
*
**/

s32 ddi_vfs_close(s32 nHandle)
{
    if( iot_fs_close_file(nHandle) < 0){
        return DDI_ERR;
    }
    return DDI_OK;       
}

/**写入文件
*@param		nHandle:		文件句柄，open_file 或 create_file 返回的
*@param		lpData:		需要写入的数据指针
*@param		nLen:		数据长度
*@return	DDI_ERR：失败 
*           DDI_EIO：文件未打开或读错误 
*           >= 0：写成功字节数。
**/

s32 ddi_vfs_write (const u8*lpData, s32 nLen, s32 nHandle)
{
    s32 result;
    result = iot_fs_write_file(nHandle,lpData,nLen);
    if(result < 0){
        return DDI_ERR;
    }
    return result;
}

/**读取文件
*@param		nHandle:		文件句柄，open_file 或 create_file 返回的
*@param		lpData:		数据保存指针
*@param		nLen:		buf长度
*@return	DDI_ERR：失败 
*           DDI_EIO：文件未打开或读错误 
*           >=0：实际读取的字节数个数。
**/
s32 ddi_vfs_read (u8 * lpData, s32 nLen, s32 nHandle)
{
    s32 result;
    result = iot_fs_read_file(nHandle, lpData, nLen);
    if(result < 0){
        return DDI_ERR;
    }
    return result; 
}

/**文件定位
*@note      参数iOffset的含义取决于iOrigin的值.
*@param		nHandle:		文件句柄，open_file 或 create_file 返回的
*@param		nOffset:	偏移量
*@param		nWhence:	SEEK_SET 0：偏移基准为文件开始处 
                        SEEK_CUR 1：偏移基准为当前偏移量 
                        SEEK_END 2：偏移基准为文件结尾处
*@return	DDI_OK：成功 
*           DDI_ERR：失败 
*           DDI_EIO：文件未打开或读错误 
**/

s32 ddi_vfs_seek(s32 nHandle, s32 nOffset, u8 nWhence)
{
    if( iot_fs_seek_file(nHandle, nOffset, nWhence) < 0){
        return DDI_ERR;
    }
    return DDI_OK;
}

/**查询文件读写位置游标
*@param		lpPath:文件句柄
*@return	DDI_ERR：失败 
*           DDI_EIO：文件未打开或读错误 
*           >=0: 文件当前读写偏移量。
**/

s32 ddi_vfs_tell (s32 nHandle)
{
    s32 result;
    result = iot_fs_seek_file(nHandle, 0, SEEK_CUR);  
    if(result < 0){
        return DDI_ERR;
    }
    return result;
}

/**删除文件
*@param		lpFileName:	文件全路径名称
*@return	DDI_OK：成功 
*           DDI_ERR：失败
**/

s32 ddi_vfs_remove(const u8 *lpFileName)
{
    if(iot_fs_delete_file(lpFileName) < 0){
        return DDI_ERR;
    }
    return DDI_OK;
}
/**重命名文件
*@param		lpOldName:	原文件名
*@param		lpNewName:	更改后的文件名
*@return	DDI_OK：成功 
*           DDI_ERR：失败
**/

s32 ddi_vfs_rename(const u8 *lpOldName, const u8 *lpNewName)
{
    if(IVTBL(rename_file)(lpOldName, lpNewName) < 0){
        return DDI_ERR;
    }
    return DDI_OK;
}

/**判断文件是否存在
*@param		lpName:	文件绝对路径
*@return	DDI_OK：成功 
*           DDI_ERR：失败
**/
s32 ddi_vfs_access(const u8 *lpName) 
{  
    int fd;
    fd = iot_fs_open_file(lpName,O_RDONLY);
    if(fd > 0)
    {
        iot_fs_close_file(fd);
        return DDI_OK;
    }
    return DDI_ERR;
}


