#ifndef __IOT_XGD_H__
#define __IOT_XGD_H__

#include "am_openat.h"
#include "string.h"
#include "iot_os.h"
#include "iot_debug.h"
#include "iot_network.h"
#include "iot_socket.h"

#define DDI_OK 0 //成功
#define DDI_ERR -1 //错误
#define DDI_ETIMEOUT -2 //超时
#define DDI_EBUSY -3 //设备繁忙
#define DDI_ENODEV -4 //设备不存在
#define DDI_EACCES -5 //无权限
#define DDI_EINVAL -6 //参数无效
#define DDI_EIO -7 //设备未打开或设备操作出错
#define DDI_EDATA -8 //数据错误
#define DDI_EPROTOCOL -9 //协议错误
#define DDI_ETRANSPORT -10 //传输错误
#define DDI_ENOMEM -11 //内存不足
#define DDI_EVERIFY -12 //校验错误
#define DDI_ESTAT -13 //状态异常
#define DDI_ENOTSUPPORT -14 //不支持的功能



typedef enum _sock_interface {
SOCK_INTERFACE_WIRELESS, //使用无线网络传输
}sock_interface_t;
typedef enum _sock_type {
SOCK_TYPE_TCP = 0,
SOCK_TYPE_UDP = 1
}sock_type_t;
typedef struct _sock_addrs {
s8 ip[46]; //IP 地址字符串，以’\0’结尾， IPv6 地址最长为 46 字节
}sock_addrs_t;
typedef enum{
SOCK_CONNSTAT_DISCONNECTED, //未连接或已断开
SOCK_CONNSTAT_CONNECTING, //正在建立连接
SOCK_CONNSTAT_CONNECTED, //已建立连接
} sock_connstat_t;


/**
 * @defgroup XGD程序及相关接口
 * @{
 */

/**
 * @defgroup 基本接口
 * @{
 */
/**@example demo_at_p/src/baseTest.c
* 
*/ 

/**系统阻塞延时
*@param		nMs：延时时间,单位:毫秒
*@return	DDI_OK: 成功
*           DDI_ERR:      失败
**/
s32 ddi_sys_msleep(u32 nMs);

/**获取一个软定时器的当前时间
*@param		void
*@return	>0:获取成功,返回软定时器的当前时间值
*           DDI_ERR:获取失败
*			备注：把本接口的返回值传递给 ddi_utils_stimer_query,用于查询是否超时。
**/
s32 ddi_utils_stimer_get(void);

/**查询从指定时间起,是否已经超时
*@param		stimer:软定时器的时间
*@param		timeout_ms:超时时间
*@return	0:未超时
*           1:已超时
*			备注:本接口的 stimer 可通过 ddi_utils_stimer_get 接口获取
**/
s32 ddi_utils_stimer_query(s32 stimer, u32 timeout_ms);

/**申请动态内存，且内存会被初始化为0x00(注意:申请的内存如果不再使用,必须显式地释放)
*@param		size:申请的内存大小,单位字节
*@return	非NULL:申请成功
*           NULL:申请失败
**/
void *ddi_mem_malloc(u32 size);


/**释放已申请的动态内存(注意:释放以后的动态内存不能再使用)
*@param		ptr:指向申请的动态内存
*@return	void
**/
void ddi_mem_free(void *ptr);
/** @}*/ 


/**
 * @defgroup socket接口
 * @{
 */
/**@example demo_at_p/src/socketTest.c
* 
*/ 


/**创建一个 socket 句柄,申请资源,并初始化相关环境
*@param		sock_type_t
*@return	>0:成功 socke 句柄
*			DDI_ERR:失败
**/
s32 ddi_socket_new(sock_type_t type);

/**释放 socket 句柄,释放资源
*@param		id:socket 句柄
*@return	DDI_OK:成功
*			DDI_EINVAL:非法参数
**/
s32 ddi_socket_free(s32 id);


/**解析域名,返回对应的 IP 地址
*@param		socket 句柄
*@param		host:待解析的域名地址,以’\0’结尾的字符串
*@param		sock_addrs_t:addrs
*@param		size:传入的’addrs’个数
*@param		timeout_ms:超时时间,单位毫秒
*@return	DDI_OK:成功
*			DDI_ERR:失败
*			DDI_ETRANSPORT:连接失败
*			DDI_ETIMEOUT:超时
**/
s32 ddi_socket_get_addr(void *ctx, const s8 *host, 
					sock_addrs_t *addrs, s32 size, s32 timeout_ms);

/**建立 socket 连接
*@param		id:socket 句柄
*@param		ipaddr:服务器 IP 地址
*@param		port:服务器端口号
*@param		timeout_ms:超时时间，单位毫秒
*@return	DDI_OK:成功
*			DDI_EINVAL:IP 地址是非法参数
*			DDI_ETRANSPORT:连接失败
*			DDI_ETIMEOUT:超时
*			备注:同一个 socket 句柄 建立连接后，必须先断开连接，才能再次建立连接
**/
s32 ddi_socket_connect(s32 id, const s8 *ipaddr, s32 port, s32 timeout_ms);

/**建立 断开 socket 连接
*@param		id:socket 句柄
*@return	DDI_OK:成功
*@return	DDI_ERR:失败
**/

s32 ddi_socket_disconnect(s32 id);


/**向对端发送数据
*@param		id:socket 句柄
*@param		buf:待发送的数据
*@param		len:数据长度
*@return	>=0:实际发送的数据长度
*			DDI_EINVAL:非法参数
*			DDI_ETRANSPORT:发送失败
*			DDI_EIO:未建立连接
**/

s32 ddi_socket_write(s32 id, const u8 *buf, s32 len);

/**从对端接收数据
*@param		id:socket 句柄
*@param		buf:缓冲区大小
*@param		len:数据长度
*@param		timeout_ms:超时时间,单位毫秒,0 表示立即返回,大于 0 表示阻塞等 数 待时 间
*@return	>=0:实际发送的数据长度
*			DDI_EINVAL:非法参数
*			DDI_ETRANSPORT:发送失败
*			DDI_EIO:未建立连接
**/

s32 ddi_socket_read(s32 id, u8 *buf, s32 len, s32 timeout_ms);

/**打开或创建文件
*@param		lpPath:文件绝对路径
*@param		lpMode:打开或创建文件模式
*"w"/"w+": 如果文件不存在则创建，存在则清空。
*"wb"/"wb+": 如果文件不存在则创建。
*"rb": 如文件不存在，返回失败。
*@return	=0:打开文件失败
*			>0 : 文件句柄
*
**/

s32 ddi_vfs_open(const u8* lpPath,const u8* lpMode);

/**关闭文件
*@param		lpPath:文件句柄
*@return	DDI_ERR: 失败
*			DDI_OK : 成功
*
**/

s32 ddi_vfs_close(s32 nHandle);

/**写入文件
*@param		nHandle:		文件句柄，open_file 或 create_file 返回的
*@param		lpData:		需要写入的数据指针
*@param		nLen:		数据长度
*@return	DDI_ERR：失败 
*           DDI_EIO：文件未打开或读错误 
*           >= 0：写成功字节数。
**/

s32 ddi_vfs_write (const u8*lpData, s32 nLen, s32 nHandle);

/**读取文件
*@param		nHandle:		文件句柄，open_file 或 create_file 返回的
*@param		lpData:		数据保存指针
*@param		nLen:		buf长度
*@return	DDI_ERR：失败 
*           DDI_EIO：文件未打开或读错误 
*           >=0：实际读取的字节数个数。
**/
s32 ddi_vfs_read (u8 * lpData, s32 nLen, s32 nHandle);

/**文件定位
*@note      参数iOffset的含义取决于iOrigin的值.
*@param		nHandle:		文件句柄，open_file 或 create_file 返回的
*@param		nOffset:	偏移量
*@param		nWhence:	SEEK_SET 0：偏移基准为文件开始处 
                        SEEK_CUR 1：偏移基准为当前偏移量 
                        SEEK_END 2：偏移基准为文件结尾处
*@return	DDI_OK：成功 
*           DDI_ERR：失败 
*           DDI_EIO：文件未打开或读错误 
**/

s32 ddi_vfs_seek(s32 nHandle, s32 nOffset, u8 nWhence);

/**查询文件读写位置游标
*@param		lpPath:文件句柄
*@return	DDI_ERR：失败 
*           DDI_EIO：文件未打开或读错误 
*           >=0: 文件当前读写偏移量。
**/

s32 ddi_vfs_tell (s32 nHandle);

/**删除文件
*@param		lpFileName:	文件全路径名称
*@return	DDI_OK：成功 
*           DDI_ERR：失败
**/

s32 ddi_vfs_remove(const u8 *lpFileName);

/**重命名文件
*@param		lpOldName:	原文件名
*@param		lpNewName:	更改后的文件名
*@return	DDI_OK：成功 
*           DDI_ERR：失败
**/

s32 ddi_vfs_rename(const u8 *lpOldName, const u8 *lpNewName);

/**判断文件是否存在
*@param		lpName:	文件绝对路径
*@return	DDI_OK：成功 
*           DDI_ERR：失败
**/
s32 ddi_vfs_access(const u8 *lpName);







/** @}*/ 

/** @}*/  //模块结尾

#endif
