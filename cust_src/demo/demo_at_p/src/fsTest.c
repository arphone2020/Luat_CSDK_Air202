#include "iot_xgd.h"
#include "string.h"
#define test_file "testFile.txt" 
#define new_test_file "newFile.txt"
#define ddi_fs_print iot_debug_print
const char *testBuf = "0000000000111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999";

//读写测试

void ddi_fs_test()
{
    int i = 1;
    char *readBuf = iot_os_malloc(20+1);

    //写测试
    ddi_fs_print("ddi_fs_test write_test start");
    //创建并打开文件
    s32 fd = ddi_vfs_open(test_file,"wb");
    ddi_fs_print("ddi_fs_test ddi_vfs_open %d",fd);
    if(fd > 0){
        //写文件
        ddi_fs_print("ddi_fs_test ddi_vfs_write %d",
                    ddi_vfs_write(testBuf,strlen(testBuf),fd));
        //关闭文件
        ddi_fs_print("ddi_fs_test ddi_vfs_close %d",
                    ddi_vfs_close(fd));
    }

    //读测试
    ddi_fs_print("ddi_fs_test read_test start");
    //读模式打开文件
    fd = ddi_vfs_open(test_file,"rb");
    ddi_fs_print("ddi_fs_test ddi_vfs_open %d",fd);
    if(fd > 0){
        //读文件
        while(i > 0){
             memset(readBuf,0,21);
             i = ddi_vfs_read(readBuf,20,fd);
             //打印读出的数据
             ddi_fs_print("ddi_fs_test ddi_vfs_read read_len: %d,read_data: %s",
                                i,readBuf); 
        }
        //iot_os_free(readBuf);
        ddi_fs_print("ddi_fs_test ddi_vfs_close %d",
                    ddi_vfs_close(fd));
    }

    
    //游标偏移测试
    ddi_fs_print("ddi_fs_test seek_test start");
    //读模式打开文件
    fd = ddi_vfs_open(test_file,"rb");
    ddi_fs_print("ddi_fs_test ddi_vfs_open %d",fd);
    if(fd > 0){
        ddi_fs_print("ddi_fs_test ddi_vfs_seek %d",ddi_vfs_seek(fd,5,SEEK_SET));
        memset(readBuf,0,21);
        i = ddi_vfs_read(readBuf,20,fd);
        //打印读出的数据
        ddi_fs_print("ddi_fs_test ddi_vfs_read read_len: %d,read_data: %s",
                                i,readBuf);
        //查询当前的游标位置
        ddi_fs_print("ddi_fs_test ddi_vfs_tell %d",ddi_vfs_tell(fd));

        ddi_fs_print("ddi_fs_test ddi_vfs_close %d",
                    ddi_vfs_close(fd));     
    }

    
    //文件名操作测试
    //重命名测试
    ddi_fs_print("ddi_fs_test ddi_vfs_rename %d",
                    ddi_vfs_rename(test_file,new_test_file));
    //判断文件是否存在
    ddi_fs_print("ddi_fs_test ddi_vfs_access start %d",
                    ddi_vfs_access(test_file));
    
    ddi_fs_print("ddi_fs_test ddi_vfs_access start %d",
                    ddi_vfs_access(new_test_file));

    //删除测试
    ddi_fs_print("ddi_fs_test ddi_vfs_remove start %d",
                    ddi_vfs_remove(new_test_file));

    iot_os_free(readBuf);
}

