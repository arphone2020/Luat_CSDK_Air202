#include "string.h"
#include "iot_debug.h"
#include "iot_lcd.h"
#include "iot_camera.h"
#include "iot_pmd.h"
#include "iot_gpio.h"
#include "demo_lcd_9341.h"
#include "demo_zbar.h"
#include "cameraCmd.h"

#define CAM_GC0310   0x10
#define CAM_ID_REG 0xf1
#define LCD_GPIO OPENAT_GPIO_3

static BOOL camera_preview_is_on = FALSE;
extern APP_STATUS g_app_status;

// camera 回调处理
void camera_evevt_callback(T_AMOPENAT_CAMERA_MESSAGE *pMsg)
{

    switch(pMsg->evtId)
    {
        case OPENAT_DRV_EVT_VIDEORECORD_FINISH_IND:
        {
            iot_debug_print("[zbar] camera_evevt_callback videorecordFinishResult %d", pMsg->param.videorecordFinishResult);  
            break;
        }
        case OPENAT_DRV_EVT_CAMERA_DATA_IND:
        {
            // 获取camera得到的数据， 发送到zbartask 去解析
            if (g_app_status == APP_STATUS_ZBAR)
            {
              lcdZbarMsgSend((unsigned char *)pMsg->dataParam.data);
            }
            break;
        }
        default:
            break;
    }
}


void camera_open(void)
{
    BOOL result;
    T_AMOPENAT_CAMERA_PARAM initParam =
    {
        camera_evevt_callback,
        OPENAT_I2C_3, 
        LCD_I2C_ADDR,
        AMOPENAT_CAMERA_REG_ADDR_8BITS|AMOPENAT_CAMERA_REG_DATA_8BITS,

        TRUE,
        FALSE,
        FALSE, 
        
        CAMERA_PREVIEW_WITHE,
        CAMERA_PREVIEW_HEIGHT,
        
        CAMERA_IMAGE_FORMAT_YUV422,
        cameraInitReg,
        sizeof(cameraInitReg)/sizeof(AMOPENAT_CAMERA_REG),
        {CAM_ID_REG, CAM_GC0310},
        {OPENAT_GPIO_19,OPENAT_GPIO_20,TRUE},
        6,
        OPENAT_SPI_MODE_MASTER2_2,
        OPENAT_SPI_OUT_V0_Y1_U0_Y0,
        TRUE // 使能camera 支持各行隔列输出
    };
 
    iot_debug_print("[zbar] CAMERA_PREVIEW_WITHE %d, HEIGHT %d", 
            CAMERA_PREVIEW_WITHE, 
            CAMERA_PREVIEW_HEIGHT);
 
    result = iot_camera_init(&initParam);
    iot_debug_print("[zbar] init result %d", result);

    result = iot_camera_poweron(FALSE);
    iot_debug_print("[zbar] poweron result %d", result);

    T_AMOPENAT_GPIO_CFG gpiocfg;

    memset(&gpiocfg, 0, sizeof(gpiocfg));
    gpiocfg.mode = OPENAT_GPIO_OUTPUT;
    iot_gpio_config(LCD_GPIO, &gpiocfg);
}

void camera_preview_close(void)
{
  camera_preview_is_on = FALSE;
  iot_gpio_set(LCD_GPIO, FALSE);
  iot_camera_preview_close();
}

BOOL  camera_preview_is_open(void)
{
  iot_debug_print("[zbar] camera_preview_is_open %d", camera_preview_is_on);
  return camera_preview_is_on;
}

BOOL camera_preview_open(void)
{
	T_AMOPENAT_CAM_PREVIEW_PARAM previewParam;
	BOOL result;

	previewParam.startX = 0;
	previewParam.startY = 0;
	previewParam.endX = LCD_PREVIEW_END_X;
	previewParam.endY = LCD_PREVIEW_END_Y;
	previewParam.offsetX = 0;
	previewParam.offsetY = 0;
	previewParam.recordAudio = FALSE;

	iot_gpio_set(LCD_GPIO, TRUE);
	
	if (!camera_preview_is_on)
	{
	  camera_preview_close();
		result = iot_camera_preview_open(&previewParam);
		iot_debug_print("[zbar] preview result %d", result);

		if (result)
		{
		  camera_preview_is_on = TRUE;
		}

		return result;
	}

	return FALSE;
}