#ifndef __DISP_H__
#define __DISP_H__

#define FONT_ESPANA_SUPPORT
#define BIG_FONT_SUPPORT
#define LUA_DISP_SUPPORT_HZ

typedef struct DispBitmapTag
{
    u16 width;
    u16 height;
    u8 bpp;
    const u8 *data;
}DispBitmap;
/*-\NEW\2013.4.10\增加黑白屏显示支持 */

typedef struct FontInfoTag
{
    u8        width;
    u8        height;
    u8        size;
    u16       start;
    u16       end;
    const u8       *data;
}FontInfo;

static const u8 blankChar[16*16/8] = {0};

// font 
// 宋体16 ascii 0x20~0x7e
static const u8 sansFont16Data[]=
{
#if defined(BIG_FONT_SUPPORT)
#include "font24.dat"
#elif defined(FONT12_SUPPORT)
#include "font12.dat"
#else
#include "font.dat"
#endif
};

static const FontInfo sansFont16 = 
{
#if defined(BIG_FONT_SUPPORT)
    12,
    24,
    (12+7)/8*24,
#elif defined(FONT12_SUPPORT)
    6,
    12,
    (6+7)/8*12,
#else
    8,
    16,
    8*16/8,
#endif
    0x20,
    0x7E,
    sansFont16Data,
};

// 汉字宋体16
#if defined(FONT_HZ_COMPRESS)
static const u8 sansHzFont16DataZip[] =
{
#include "fonthz.zip.dat"
};

static const u8 *sansHzFont16Data = NULL;
#else
static const u8 sansHzFont16Data[] =
{
#if defined(BIG_FONT_SUPPORT)
#include "fonthz24dat"
#elif defined(FONT12_SUPPORT)
#include "fonthz12.dat"
#else
#if defined(AM_FONTHZ_TWO_LEVEL_SUPPORT)
#include "fonthzTwoLevel.dat"
#else
#include "fonthz.dat"
#endif
#endif
};

/*+\NEW\liweiqiang\2013.12.18\增加中文标点符号的显示支持 */
static const u8 sansHzFont16ExtData[] = 
{
#if defined(FONT12_SUPPORT)
    #include "fonthzext12.dat"
#else
    #include "fonthzext.dat"
#endif
};

/*按内码由小到大排列*/
static const u16 sansHzFont16ExtOffset[] =
{
//"、。—…‘’“”〔〕〈〉《》「」『』【】！（），－．：；？嗯"
    0xA1A2,0xA1A3,0xA1AA,0xA1AD,0xA1AE,0xA1AF,0xA1B0,0xA1B1,
    0xA1B2,0xA1B3,0xA1B4,0xA1B5,0xA1B6,0xA1B7,0xA1B8,0xA1B9,
    0xA1BA,0xA1BB,0xA1BE,0xA1BF,0xA3A1,0xA3A8,0xA3A9,0xA3AC,
    0xA3AD,0xA3AE,0xA3BA,0xA3BB,0xA3BF,0xE0C5
};
/*-\NEW\liweiqiang\2013.12.18\增加中文标点符号的显示支持 */
#endif

/*+\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */
#if defined(FONT_ESPANA_SUPPORT)
static const u8 EsFont16Data[] = 
{
    #include "frontes.dat"
};

/*按内码由小到大排列*/
static const u16 EsFont16Offset[] =
{
//"!?náéíóúü"
    0xA1,0xBF,0xF1,0xA8A2,0xA8A6,0xA8AA,0xA8AE,0xA8B2,0xA8B9
};

static FontInfo sansHzFontEs =
{
    8,
    16,
    (8+7)/8*16,
    0,
    0,
    NULL
};
#endif
/*-\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */

static FontInfo sansHzFont16 =
{
#if defined(BIG_FONT_SUPPORT)
    24,
    24,
    (24+7)/8*24,
#elif defined(FONT12_SUPPORT)
    12,
    12,
    (12+7)/8*12,
#else
    16,
    16,
    (16+7)/8*16,
#endif
    0,
    0,
    NULL
};

void disp_puttext(const char *string, u16 x, u16 y);
void disp_init(void);

void disp_zbar_test_begin(void);
void disp_zbar_test_end(char *data);
void disp_keypad_test_begin(void);
void disp_keypad_test_process(char *count);
void disp_keypad_test_end(BOOL end);
int disp_centere_x(char *data);
int disp_clear_x(int x, int y, int len);


#endif
