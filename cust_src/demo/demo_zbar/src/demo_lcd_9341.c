
#include "string.h"
#include "iot_debug.h"
#include "iot_lcd.h"
#include "iot_camera.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "demo_lcd_9341.h"
#include "demo_zbar.h"


#ifdef LCD_9341
#define LCD_RST_PIN OPENAT_GPIO_14
#define LCD_RS_PIN OPENAT_GPIO_18
#define LCD_BL_PIN OPENAT_GPIO_2
#define LCD_WIDTH 240
#define LCD_HEIGHT 320
u16 lcd_y_offset = 0;
u16 lcd_x_offset = 0;

const int initcmd[] =
{
	0x11,
	0x00010078,
	0xCF,
	0x00030000,
	0x00030099,
	0x00030030,
	0xED,
	0x00030064,
	0x00030003,
	0x00030012,
	0x00030081,
	0xCB,
	0x00030039,
	0x0003002C,
	0x00030000,
	0x00030034,
	0x00030002,
	0xEA,
	0x00030000,
	0x00030000,
	0xE8,
	0x00030085,
	0x00030000,
	0x00030078,
	0xC0,
	0x00030023,
	0xC1,
	0x00030012,
	0xC2,
	0x00030011,
	0xC5,
	0x00030040,
	0x00030030,
	0xC7,
	0x000300A9,
	0x3A,
	0x00030055,
	0x36,
	0x00030008,
	0xB1,
	0x00030000,
	0x00030018,
	0xB6,
	0x0003000A,
	0x000300A2,
	0xF2,
	0x00030000,
	0xF7,
	0x00030020,
	0x26,
	0x00030001,
	0xE0,
	0x0003001F,
	0x00030024,
	0x00030023,
	0x0003000B,
	0x0003000F,
	0x00030008,
	0x00030050,
	0x000300D8,
	0x0003003B,
	0x00030008,
	0x0003000A,
	0x00030000,
	0x00030000,
	0x00030000,
	0x00030000,
	0xE1,
	0x00030000,
	0x0003001B,
	0x0003001C,
	0x00030004,
	0x00030010,
	0x00030007,
	0x0003002F,
	0x00030027,
	0x00030044,
	0x00030007,
	0x00030015,
	0x0003000F,
	0x0003003F,
	0x0003003F,
	0x0003001F,			
	0x29,
};

static void write_command_table(const int *table, int size)
{
    UINT16 flag;
    UINT16 value;
    UINT16 index;

    for(index = 0; index < size && table[index] != (UINT32)-1; index++)
    {
        flag = table[index]>>16;
        value = table[index]&0xffff;

        switch(flag)
        {
            case 1:
                iot_os_sleep(value);
                break;
            case 0:
            case 2:
                iot_lcd_write_cmd(value&0x00ff);
                break;

            case 3:
                iot_lcd_write_data(value&0x00ff);
                break;

            default:
                break;
        }
    }
}

static void lcd_reg_init(void)
{
    write_command_table(initcmd, sizeof(initcmd)/sizeof(int));
}

void lcdSetWindowAddress(T_AMOPENAT_LCD_RECT_T *pRect)
{
    u16 ltx = pRect->ltX + lcd_x_offset;
    u16 lty = pRect->ltY + lcd_y_offset;
    u16 rbx = pRect->rbX + lcd_x_offset;
    u16 rby = pRect->rbY + lcd_y_offset;
	
    iot_lcd_write_cmd(0x2a);    //Set Column Address
    iot_lcd_write_data(ltx>>8);
    iot_lcd_write_data(ltx&0x00ff);        
    iot_lcd_write_data(rbx>>8);
    iot_lcd_write_data(rbx&0x00ff);
    
    iot_lcd_write_cmd(0x2b);    //Set Page Address
    iot_lcd_write_data(lty>>8);
    iot_lcd_write_data(lty&0x00ff);                
    iot_lcd_write_data(rby>>8);
    iot_lcd_write_data(rby&0x00ff);
    
    // Write the display data into GRAM here 
    iot_lcd_write_cmd(0x2C); //GRAM start writing 
    
}

// update 
void lcdMsgCallback(T_AMOPENAT_LCD_MESSAGE *pMsg)
{    
    switch(pMsg->evtId)
    {
        case OPENAT_DRV_EVT_LCD_REFRESH_REQ:
            {
                T_AMOPENAT_LCD_REFRESH_REQ *pRefreshReq = &pMsg->param.refreshReq;

                // update lcd显示camera数据
                lcdSetWindowAddress(&pRefreshReq->rect);
                iot_lcd_update_color_screen(&pRefreshReq->rect, NULL);
            }
            break;

        default:
            break;
    }
}

void lcd_led(void)
{
    T_AMOPENAT_GPIO_CFG cfg;

    memset(&cfg, 0, sizeof(T_AMOPENAT_GPIO_CFG));
    cfg.mode = OPENAT_GPIO_OUTPUT;
    cfg.param.defaultState = 1;
    iot_gpio_config(LCD_BL_PIN, &cfg);
    lcd_reg_init();
}

void lcd_open(void)
{  
    BOOL ret;
    T_AMOPENAT_COLOR_LCD_PARAM param;
    
    iot_pmd_poweron_ldo(OPENAT_LDO_POWER_LCD,6);
    iot_pmd_poweron_ldo(OPENAT_LDO_POWER_VLCD,6);

    //初始化LCD
    param.width = LCD_WIDTH;
    param.height = LCD_HEIGHT;
    param.msgCallback = lcdMsgCallback;
    param.bus = OPENAT_LCD_SPI4LINE; // 设置lcd接口为spi接口
    param.lcdItf.spi.frequence = 26000000;
    param.lcdItf.spi.csPort = OPENAT_GPIO_UNKNOWN; //无自定义cs脚则设为unknown
    param.lcdItf.spi.rstPort = LCD_RST_PIN;
    ret = iot_lcd_color_init(&param);
    iot_debug_print("[zbar] lcd init result %d", ret);

    // 打开背光
    lcd_led();
	
	
}
#endif


// LCD spi的pin脚功能，gpoi和lcd spi来回切换测试

static VOID demo_gpio_handle (E_OPENAT_DRV_EVT evt, 
                    E_AMOPENAT_GPIO_PORT gpioPort,
                unsigned char state)
{
    UINT8 status;

    // 判断是gpio中断
    if (OPENAT_DRV_EVT_GPIO_INT_IND == evt)
    {
        
        // 触发电平的状态
        iot_debug_print("[zbar gpio] input handle gpio %d, state %d", gpioPort, state);

        // 读当前gpio状态, 1:高电平 0:低电平
        iot_gpio_read(gpioPort, &status);
        iot_debug_print("[zbar gpio] input handle gpio %d, status %d", gpioPort, state);
            
    }
}


static void lcd_gpoiReConfig(void)
{
	#define LCD_RSTB_GPIO14 (14)
	#define LCD_CS_GPIO15 (15)
	#define LCD_CLK_GPIO16 (16)
	#define LCD_DIO_GPIO17 (17) 
	#define LCD_DSC_GPIO18 (18)

	T_AMOPENAT_GPIO_CFG  input_cfg;
    BOOL err;
    
    memset(&input_cfg, 0, sizeof(T_AMOPENAT_GPIO_CFG));
    
    input_cfg.mode = OPENAT_GPIO_INPUT_INT; //配置输入中断
    input_cfg.param.defaultState = FALSE;    
    input_cfg.param.intCfg.debounce = 50;  //防抖50ms
    input_cfg.param.intCfg.intType = OPENAT_GPIO_INT_BOTH_EDGE; //中断触发方式双边沿
    input_cfg.param.intCfg.intCb = demo_gpio_handle; //中断处理函数

	iot_gpio_close(LCD_RSTB_GPIO14);
	err = iot_gpio_config(LCD_RSTB_GPIO14, &input_cfg);
	iot_debug_print("[zbar] LCD_RSTB_GPIO14 input err %d", err);

	iot_gpio_close(LCD_CS_GPIO15);
	err = iot_gpio_config(LCD_CS_GPIO15, &input_cfg);
	iot_debug_print("[zbar] LCD_CS_GPIO15 input err %d", err);

	iot_gpio_close(LCD_CLK_GPIO16);
	err = iot_gpio_config(LCD_CLK_GPIO16, &input_cfg);
	iot_debug_print("[zbar] LCD_CLK_GPIO16 input err %d", err);

	iot_gpio_close(LCD_DIO_GPIO17);
	err = iot_gpio_config(LCD_DIO_GPIO17, &input_cfg);
	iot_debug_print("[zbar] LCD_DIO_GPIO17 input err %d", err);

	iot_gpio_close(LCD_DSC_GPIO18);
	err = iot_gpio_config(LCD_DSC_GPIO18, &input_cfg);
	iot_debug_print("[zbar] LCD_DSC_GPIO18 input err %d", err);
	
}


static void lcd_reOpen(void)
{
	BOOL ret;
	UINT32 total;
    UINT32 used; 
	
	iot_os_mem_used(&total, &used);
	iot_debug_print("[zbar] lcd_reOpen iot_lcd_close %d,%d", total, used);
	iot_lcd_close();
	lcd_open();
    iot_os_mem_used(&total, &used);
	iot_debug_print("[zbar] lcd_reOpen lcd_open %d,%d", total, used);
}

void lcd_pinSwitch(void)
{
	int i;
	
	for(i=0; i<100000; i++)
	{
		lcd_gpoiReConfig();
		iot_os_sleep(2000);
		lcd_reOpen();
		iot_os_sleep(2000);
		iot_debug_print("[zbar] lcd_reOpen count %d", i);
	}
}

