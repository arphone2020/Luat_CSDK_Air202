#include "adpcm.h"
#include "iot_debug.h"

#define CLIP(data, min, max) \
if ((data) > (max)) data = max; \
else if ((data) < (min)) data = min;
#define NOISE_SHAPING_OFF       0   // flat noise (no shaping)
#define NOISE_SHAPING_STATIC    1   // first-order highpass shaping
#define NOISE_SHAPING_DYNAMIC   2   // dynamically tilted noise based on signal


/* step index tables */
static const int indexTable[] = {
    /* adpcm data size is 4 */
    -1, -1, -1, -1, 2, 4, 6, 8
};


static const int index_table[16] =
{
    -1, -1, -1, -1, 2, 4, 6, 8,
    -1, -1, -1, -1, 2, 4, 6, 8,
};

/**
 * This is the step table. Note that many programs use slight deviations from
 * this table, but such deviations are negligible:
 */
static const int step_table[89] =
{
    7, 8, 9, 10, 11, 12, 13, 14, 16, 17,
    19, 21, 23, 25, 28, 31, 34, 37, 41, 45,
    50, 55, 60, 66, 73, 80, 88, 97, 107, 118,
    130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
    337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
    876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066,
    2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
    5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
    15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767
};

void adpcm_decoder(char* inbuff,char* outbuff,int len_of_in,struct adpcm_state *state )
{
	int  i=0,j=0;
    char tmp_data;
    struct adpcm_state *tmp_state =state;
    long step;/* Quantizer step size */
    signed long predsample;/* Output of ADPCM predictor */
    signed long diffq;/* Dequantized predicted difference */
    int index;/* Index into step size table */

    int Samp;
    unsigned char SampH,SampL;
    unsigned char inCode;
 
    /* Restore previous values of predicted sample and quantizer step
    size index
    */
    predsample =state->valprev;
    index =state->index;
 
    for(i=0;i<len_of_in*2;i++)
    {
    	tmp_data=inbuff[i/2];
    	if(i%2)
    	{
    		inCode=(tmp_data&0xf0)>>4;
    	}
    	else
    	{
    		inCode=tmp_data &0x0f;
    	}
    
    	step =step_table[index];
   /* Inverse quantize the ADPCM code into a predicted difference
    using the quantizer step size
   */
    
    	diffq =step >>3;
    	if(inCode &4)
    	{
    		diffq +=step;
    	}
    	if(inCode &2)
    	{
    		diffq +=step >>1;
    	}
    	if(inCode &1)
    	{
    		diffq +=step >>2;
    	}
    /* Fixed predictor computes new predicted sample by adding the
    old predicted sample to predicted difference
    */
    	if(inCode &8)
    	{
    		predsample -=diffq;
    	}
    	else
    	{
    		predsample +=diffq;
    	}
    /* Check for overflow of the new predicted sample
    */
    	if(predsample >32767)
    	{
    		predsample =32767;
    	}
    	else if(predsample <-32768)
    	{
    		predsample =-32768;
    	}
    /* Find new quantizer stepsize index by adding the old index
    to a table lookup using the ADPCM code
    */
    	index +=index_table[inCode];
    /* Check for overflow of the new quantizer step size index
    */
    	if(index <0)
    		index =0;
    	if(index >88)
    		index =88;
    /* Return the new ADPCM code */
    	Samp=predsample;
    	if(Samp>=0)
    	{
    		SampH=Samp/256;
    		SampL=Samp-256*SampH;
    	}
    	else
    	{
			Samp=32768+Samp;
			SampH=Samp/256;
			SampL=Samp-256*SampH;
			SampH+=0x80;
    	}
    	outbuff[j++]=SampL;
    	outbuff[j++]=SampH;
	}
 
 /* Save the predicted sample and quantizer step size index for
 next iteration
 */
	state->valprev =(short)predsample;
	state->index =(char)index;
}

static double minimum_error (const struct adpcm_channel *pchan, int nch, int32_t csample, const int16_t *sample, int depth, int *best_nibble)
{
    int32_t delta = csample - pchan->pcmdata;
    struct adpcm_channel chan = *pchan;
    int step = (int)step_table[chan.index];
    int trial_delta = (step >> 3);
    int nibble, nibble2;
    double min_error;

    if (delta < 0) {
        int mag = (-delta << 2) / step;
        nibble = 0x8 | (mag > 7 ? 7 : mag);
    }
    else {
        int mag = (delta << 2) / step;
        nibble = mag > 7 ? 7 : mag;
    }

    if (nibble & 1) trial_delta += (step >> 2);
    if (nibble & 2) trial_delta += (step >> 1);
    if (nibble & 4) trial_delta += step;
    if (nibble & 8) trial_delta = -trial_delta;

    chan.pcmdata += trial_delta;
    CLIP(chan.pcmdata, -32768, 32767);
    if (best_nibble) *best_nibble = nibble;
    min_error = (double) (chan.pcmdata - csample) * (chan.pcmdata - csample);

    if (depth) {
        chan.index += indexTable[nibble & 0x07];
        CLIP(chan.index, 0, 88);
        min_error += minimum_error (&chan, nch, sample [nch], sample + nch, depth - 1, NULL);
    }
    else
        return min_error;

    for (nibble2 = 0; nibble2 <= 0xF; ++nibble2) {
        double error;

        if (nibble2 == nibble)
            continue;

        chan = *pchan;
        trial_delta = (step >> 3);

        if (nibble2 & 1) trial_delta += (step >> 2);
        if (nibble2 & 2) trial_delta += (step >> 1);
        if (nibble2 & 4) trial_delta += step;
        if (nibble2 & 8) trial_delta = -trial_delta;

        chan.pcmdata += trial_delta;
        CLIP(chan.pcmdata, -32768, 32767);

        error = (double) (chan.pcmdata - csample) * (chan.pcmdata - csample);

        if (error < min_error) {
            chan.index += indexTable[nibble2 & 0x07];
            CLIP(chan.index, 0, 88);
            error += minimum_error (&chan, nch, sample [nch], sample + nch, depth - 1, NULL);

            if (error < min_error) {
                if (best_nibble) *best_nibble = nibble2;
                min_error = error;
            }
        }
    }

    return min_error;
}

static uint8_t adpcm_encode_sample (struct adpcm_context *pcnxt, int ch, const int16_t *sample, int num_samples)
{
    struct adpcm_channel *pchan = pcnxt->channels + ch;
    int32_t csample = *sample;
    int depth = num_samples - 1, nibble;
    int step = step_table[pchan->index];
    int trial_delta = (step >> 3);

    if (pcnxt->noise_shaping == NOISE_SHAPING_DYNAMIC) {
        int32_t sam = (3 * pchan->history [0] - pchan->history [1]) >> 1;
        int32_t temp = csample - (((pchan->weight * sam) + 512) >> 10);
        int32_t shaping_weight;

        if (sam && temp) pchan->weight -= (((sam ^ temp) >> 29) & 4) - 2;
        pchan->history [1] = pchan->history [0];
        pchan->history [0] = csample;

        shaping_weight = (pchan->weight < 256) ? 1024 : 1536 - (pchan->weight * 2);
        temp = -((shaping_weight * pchan->error + 512) >> 10);

        if (shaping_weight < 0 && temp) {
            if (temp == pchan->error)
                temp = (temp < 0) ? temp + 1 : temp - 1;

            pchan->error = -csample;
            csample += temp;
        }
        else
            pchan->error = -(csample += temp);
    }
    else if (pcnxt->noise_shaping == NOISE_SHAPING_STATIC)
        pchan->error = -(csample -= pchan->error);

    if (depth > pcnxt->lookahead)
        depth = pcnxt->lookahead;

    minimum_error (pchan, pcnxt->num_channels, csample, sample, depth, &nibble);

    if (nibble & 1) trial_delta += (step >> 2);
    if (nibble & 2) trial_delta += (step >> 1);
    if (nibble & 4) trial_delta += step;
    if (nibble & 8) trial_delta = -trial_delta;

    pchan->pcmdata += trial_delta;
    pchan->index += indexTable[nibble & 0x07];
    CLIP(pchan->index, 0, 88);
    CLIP(pchan->pcmdata, -32768, 32767);

    if (pcnxt->noise_shaping)
        pchan->error += pchan->pcmdata;

    return nibble;
}
/*
multiple of 2 samples
*/
void adpcm_encode_chunks (struct adpcm_context *pcnxt, uint8_t *outbuf, uint32_t *outbufsize, int16_t *inbuf, uint32_t *inbufcount)
{
	const int16_t *pcmbuf;
	int chunks, ch;
	int16_t *pIn= inbuf;
	uint8_t *pOut= outbuf;

	if(pcnxt->tmp_count==1){
		//only for 1 ch
		*pOut = adpcm_encode_sample (pcnxt, 0, pcnxt->tmp_buf, 1);
		*pOut |= adpcm_encode_sample (pcnxt, 0, pIn,1) << 4;

		(pIn)++;
		(pOut)++;
		*outbufsize += 1;
		*inbufcount -= 1;
		pcnxt->block_samples +=2;
	}else if(pcnxt->tmp_count==2){
		//only for 1 ch
		*pOut = adpcm_encode_sample (pcnxt, 0, pcnxt->tmp_buf, 1);
		*pOut |= adpcm_encode_sample (pcnxt, 0, pcnxt->tmp_buf+1,1) << 4;

		(pOut)++;
		*outbufsize += 1;
		pcnxt->block_samples +=2;
	}

	pcnxt->tmp_count =0;

	if(pcnxt->block_samples==505)//one block
		return;

	if((*inbufcount) <= 505- pcnxt->block_samples ){
		chunks = (*inbufcount)>>1;
		*outbufsize += chunks * pcnxt->num_channels;
		*inbufcount = (*inbufcount) - (chunks*2);
	}else{
		chunks = (505- pcnxt->block_samples)>>1;
		*outbufsize += chunks * pcnxt->num_channels;
		*inbufcount = (*inbufcount) - (chunks*2);
	}
	pcnxt->block_samples +=chunks*2;

	while (chunks-- >0)
	{
		for (ch = 0; ch < pcnxt->num_channels; ch++)
		{
			pcmbuf = pIn + ch;

			*pOut = adpcm_encode_sample (pcnxt, ch, pcmbuf, chunks *2 +2);
			pcmbuf += pcnxt->num_channels;
			*pOut |= adpcm_encode_sample (pcnxt, ch, pcmbuf, chunks * 2 + 1) << 4;
			pcmbuf += pcnxt->num_channels;
			(pOut)++;
		}
		pIn += 2 * pcnxt->num_channels;
	}

	if((*inbufcount) ==1){
		pcnxt->tmp_buf[0] = pIn[0];
		pcnxt->tmp_count =1;
		*inbufcount -= 1;
	}
}

void adpcm_encode_block_header(struct adpcm_context *pcnxt, uint8_t *outbuf, uint32_t *outbufsize, const int16_t *inbuf, uint32_t *inbufcount)
{
	if(pcnxt->tmp_count>0){
		//2 byte for first pcm value.
		*((int16_t*)outbuf)= pcnxt->tmp_buf[0];
		pcnxt->tmp_count--;
		if(pcnxt->tmp_count>0)//move data
			pcnxt->tmp_buf[0]=pcnxt->tmp_buf[1];
	}else{
		//2 byte for first pcm value.
		*((int16_t*)outbuf)= *((int16_t*)inbuf);
		//update input buffer count.
		*inbufcount -=1;
	}
	outbuf[2]= pcnxt->channels[0].index;
	outbuf[3]= 0;
	//only for 1 ch
	pcnxt->channels[0].pcmdata= *((int16_t*)outbuf);
	//update
	*outbufsize +=4;
	pcnxt->block_samples =1;//total 505
	return;
}
